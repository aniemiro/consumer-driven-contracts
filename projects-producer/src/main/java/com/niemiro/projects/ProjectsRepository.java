package com.niemiro.projects;

import com.niemiro.projects.dto.ProjectDto;

import java.util.List;

public interface ProjectsRepository
{
    List<ProjectDto> getProjects();
}
