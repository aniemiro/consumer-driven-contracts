package com.niemiro.projects.infrastructure.repository;

import com.niemiro.projects.ProjectsRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ProjectsRepositoryConfiguration
{
    @Bean
    ProjectsRepository projectsRepository()
    {
        return new InMemoryProjectsRepository();
    }
}
