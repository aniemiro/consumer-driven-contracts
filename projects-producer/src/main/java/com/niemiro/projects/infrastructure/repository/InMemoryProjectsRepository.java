package com.niemiro.projects.infrastructure.repository;

import com.niemiro.projects.ProjectsRepository;
import com.niemiro.projects.dto.ProjectDto;

import java.util.List;
import java.util.Map;

class InMemoryProjectsRepository implements ProjectsRepository
{
    private final Map<Long, ProjectDto> db = Map.of(1L, new ProjectDto(1L, "test project 1"),
                                                    2L, new ProjectDto(2L, "test project 2"));


    @Override
    public List<ProjectDto> getProjects()
    {
        return db.values().stream()
            .toList();
    }
}
