package com.niemiro.projects.infrastructure;

import com.niemiro.projects.ProjectsFacade;
import com.niemiro.projects.ProjectsFacadeFactory;
import com.niemiro.projects.ProjectsRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ProjectsConfiguration
{
    @Bean
    ProjectsFacade projectsFacade(ProjectsRepository projectsRepository)
    {
        return ProjectsFacadeFactory.projectsFacade(projectsRepository);
    }
}
