package com.niemiro.projects.infrastructure.rest;

import com.niemiro.projects.ProjectsFacade;
import com.niemiro.projects.dto.ProjectDto;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
@AllArgsConstructor
public class ProjectsController
{
    ProjectsFacade projectsFacade;

    @GetMapping
    public List<ProjectDto> getProjects()
    {
        return projectsFacade.getProjects();
    }
}
