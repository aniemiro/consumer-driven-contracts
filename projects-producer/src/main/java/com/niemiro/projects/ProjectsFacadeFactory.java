package com.niemiro.projects;

public class ProjectsFacadeFactory
{
    public static ProjectsFacade projectsFacade(ProjectsRepository repository)
    {
        ProjectsFinder finder = new ProjectsFinder(repository);
        return new ProjectsFacade(finder);
    }
}
