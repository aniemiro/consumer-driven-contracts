package com.niemiro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectsProducerApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(ProjectsProducerApplication.class, args);
    }
}
