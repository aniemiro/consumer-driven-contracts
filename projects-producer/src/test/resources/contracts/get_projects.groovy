import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "should return all Projects"

    request {
        url "/api/projects"
        method GET()
    }

    response {
        status OK()
        headers {
            contentType applicationJson()
        }
        body ([
                [
                        id: 1,
                        name: "test project 1"
                ],
                [
                        id: 2,
                        name: "test project 2"
                ]
        ])
    }
}
