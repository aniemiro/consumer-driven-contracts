package com.niemiro.projects.contracts;

import com.niemiro.ProjectsProducerApplication;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@ActiveProfiles("test")
@SpringBootTest
@ContextConfiguration(classes = { ProjectsProducerApplication.class })
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BaseContractTest
{
    @Autowired
    private MockMvc mockMvc;

    @BeforeAll
    public void setupAll()
    {
        RestAssuredMockMvc.mockMvc(mockMvc);
    }
}
