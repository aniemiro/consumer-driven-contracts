package com.niemiro.consumer;

import com.niemiro.consumer.projects.ProjectsClient;

public class ConsumerFacadeFactory
{
    public static ConsumerFacade consumerFacade(ProjectsClient projectsClient)
    {
        ConsumerProjectsService projectsService = new ConsumerProjectsService(projectsClient);
        return new ConsumerFacade(projectsService);
    }
}
