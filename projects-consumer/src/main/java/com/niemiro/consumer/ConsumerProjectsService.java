package com.niemiro.consumer;

import com.niemiro.consumer.dto.ConsumerInfoDto;
import com.niemiro.consumer.projects.ProjectDto;
import com.niemiro.consumer.projects.ProjectsClient;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
class ConsumerProjectsService
{
    private ProjectsClient projectsClient;

    public ConsumerInfoDto getInfo()
    {
        List<ProjectDto> projects = projectsClient.getProjects();
        return new ConsumerInfoDto(projects != null ? projects.size() : 0);
    }
}
