package com.niemiro.consumer.infrastructure.projects;

import com.niemiro.consumer.projects.ProjectsClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
class ProjectsClientConfiguration
{
    @Bean
    ProjectsClient projectsClient()
    {
        return new RestTemplateProjectsClient(new RestTemplate(), "");
    }
}
