package com.niemiro.consumer.infrastructure.projects;

import com.niemiro.consumer.projects.ProjectDto;
import com.niemiro.consumer.projects.ProjectsClient;
import lombok.AllArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@AllArgsConstructor
class RestTemplateProjectsClient implements ProjectsClient
{
    private RestTemplate restTemplate;
    private String projectsUrl;

    @Override
    public List<ProjectDto> getProjects()
    {
        ResponseEntity<List<ProjectDto>> response = restTemplate.exchange(
            projectsUrl + "/api/projects",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<>()
            {
            }
        );

        return response.getBody();
    }
}
