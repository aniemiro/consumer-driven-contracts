package com.niemiro.consumer.infrastructure;

import com.niemiro.consumer.ConsumerFacade;
import com.niemiro.consumer.ConsumerFacadeFactory;
import com.niemiro.consumer.projects.ProjectsClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ConsumerConfiguration
{
    @Bean
    ConsumerFacade consumerFacade(ProjectsClient projectsClient)
    {
        return ConsumerFacadeFactory.consumerFacade(projectsClient);
    }
}
