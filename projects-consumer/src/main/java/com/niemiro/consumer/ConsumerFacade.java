package com.niemiro.consumer;

import com.niemiro.consumer.dto.ConsumerInfoDto;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ConsumerFacade
{
    private ConsumerProjectsService consumerProjectsService;

    public ConsumerInfoDto getInfo()
    {
        return consumerProjectsService.getInfo();
    }
}
