package com.niemiro.consumer.projects;

import java.util.List;

public interface ProjectsClient
{
    List<ProjectDto> getProjects();
}
