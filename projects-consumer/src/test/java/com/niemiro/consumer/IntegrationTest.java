package com.niemiro.consumer;

import com.niemiro.ConsumerApplication;
import com.niemiro.consumer.dto.ConsumerInfoDto;
import com.niemiro.consumer.infrastructure.projects.ProjectsClientStubConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.junit.jupiter.Testcontainers;

@ActiveProfiles("test")
@SpringBootTest
@ContextConfiguration(classes = { ConsumerApplication.class, ProjectsClientStubConfiguration.class})
@Testcontainers
@AutoConfigureStubRunner(
    stubsMode = StubRunnerProperties.StubsMode.LOCAL,
    ids = "com.niemiro:projects-producer:0.0.1-SNAPSHOT"
)
@Slf4j
class IntegrationTest
{
    @Autowired
    ConsumerFacade consumerFacade;

    @Test
    public void test()
    {
        // given stub from projects-producer which returns 2 projects from getProjects() endpoint

        // when
        ConsumerInfoDto info = consumerFacade.getInfo();

        // then
        Assertions.assertNotNull(info);
        Assertions.assertEquals(2, info.getNumOfProjects());
    }
}
