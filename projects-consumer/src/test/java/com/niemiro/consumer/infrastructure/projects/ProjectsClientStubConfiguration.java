package com.niemiro.consumer.infrastructure.projects;

import com.niemiro.consumer.projects.ProjectsClient;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

@TestConfiguration
public class ProjectsClientStubConfiguration
{
    @StubRunnerPort("projects-producer")
    int wiremockPort;

    @Bean
    @Primary
    ProjectsClient projectsClientTestBean()
    {
        return new RestTemplateProjectsClient(new RestTemplate(), "http://localhost:" + wiremockPort);
    }
}
